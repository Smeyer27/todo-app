package com.example.todo.util

import android.R
import android.app.Activity
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.model.models.Todo
import com.example.todo.views.adapter.TodoAdapter
import com.google.android.material.snackbar.Snackbar
import java.util.*

var listIDs = mutableListOf<String>()

fun Fragment.loggerToastFrag(msg: String) {
    Log.d("LOGD", msg)
    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
}

fun Activity.loggerToastAct(msg: String) {
    Log.d("LOGD", msg)
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}


var simpleCallback = object : ItemTouchHelper.SimpleCallback(
    ItemTouchHelper.LEFT.or(
        ItemTouchHelper.UP.or(
            ItemTouchHelper.DOWN.or(
                ItemTouchHelper.DOWN))), 0 )
{

    private lateinit var recyclerAdapter : TodoAdapter
    private lateinit var recyclerView: RecyclerView

    lateinit var todoList : MutableList<Todo>
    lateinit var deletedItem : Todo

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        val startPosition = viewHolder.adapterPosition
        val endPosition = target.adapterPosition

        Collections.swap(todoList, startPosition, endPosition)
        recyclerView.adapter?.notifyItemMoved(startPosition, endPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition

        when (direction) {
            ItemTouchHelper.LEFT -> {
                deletedItem = todoList[position]
                todoList.drop(position)
                recyclerAdapter.notifyItemRemoved(position)

                Snackbar.make(recyclerView, "$deletedItem is deleted", Snackbar.LENGTH_LONG).setAction("Undo", View.OnClickListener {
                    todoList.add(position, deletedItem)
                    recyclerAdapter.notifyItemInserted(position)
                }).show()
            }
        }
    }
}
