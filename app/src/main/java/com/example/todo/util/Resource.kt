package com.example.todo.util

sealed class Resource<T>(data:T? = null, msg: String?) {
    data class Success<T>(val data: T) : Resource<T>(data, null)
    class Loading<T> : Resource<T>(null, null)
    data class Error<T>(val msg: String) : Resource<T>(null, msg)
    class Idle<T> : Resource<T>(null, null)
}
