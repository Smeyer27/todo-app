package com.example.todo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import com.example.todo.views.adapter.TodoAdapter
import com.example.todo.views.frags.TodoListFragment
import java.util.*

class MainActivity : AppCompatActivity(R.layout.activity_main)
