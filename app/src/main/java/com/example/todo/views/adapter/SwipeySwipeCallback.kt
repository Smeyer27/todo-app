package com.example.todo.views.adapter

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.viewmodels.TodoListViewModel

class SwipeySwipeCallback (private val adapter: TodoAdapter, val viewmodel: TodoListViewModel)

 : ItemTouchHelper.SimpleCallback(
    ItemTouchHelper.LEFT.or(
        ItemTouchHelper.UP.or(
            ItemTouchHelper.DOWN.or(
                ItemTouchHelper.DOWN))), 0 )
{


    private lateinit var recyclerAdapter : TodoAdapter
    private lateinit var recyclerView: RecyclerView


    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        return makeMovementFlags(0, ItemTouchHelper.START)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val todo = adapter.todoList[viewHolder.adapterPosition]
        adapter.deleteTodo(todo)
//        val position = viewHolder.adapterPosition
//
//        when (direction) {
//            ItemTouchHelper.LEFT -> {
//                val deletedItem  = todo
//                adapter.todoList.drop(position)
//                recyclerAdapter.deleteTodo(todo)

//                Snackbar.make(recyclerView, "$deletedItem is deleted", Snackbar.LENGTH_LONG).setAction("Undo", View.OnClickListener {
//                    adapter.todoList.add(position, deletedItem)
//                    recyclerAdapter.notifyItemInserted(position)
//                }).show()
//            }
//        }
    }
}
