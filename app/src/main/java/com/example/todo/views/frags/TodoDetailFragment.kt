package com.example.todo.views.frags

import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.todo.databinding.FragmentTodoDetailBinding
import com.example.todo.model.repo.RepoImplement
import com.example.todo.model.models.Todo
import com.example.todo.viewmodels.TodoDetailViewModel
import com.example.todo.viewmodels.factories.ViewmodelFactoryDetail
import java.time.Clock
import java.time.Instant

class TodoDetailFragment : Fragment() {

    private var _binding: FragmentTodoDetailBinding? = null
    private val binding: FragmentTodoDetailBinding get() = _binding!!

    private val repoImp by lazy { RepoImplement(requireContext()) }
    private val viewmodel by viewModels<TodoDetailViewModel> { ViewmodelFactoryDetail(repoImp) }
    private val args by navArgs<TodoDetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentTodoDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)

        if (args.toDoItem != null) {
            viewmodel.title = args.toDoItem?.title.toString()
            viewmodel.desc = args.toDoItem?.description.toString()
            addEditBtn.text = "Edit Item"
            todoTitle.text = SpannableStringBuilder(args.toDoItem!!.title)
            todoDescription.text = SpannableStringBuilder(args.toDoItem!!.description)
        }
        else {
            addEditBtn.text = "Add Item"
        }

        initListeners()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initListeners() = with(binding) {

        viewmodel.continueLiveData.observe(viewLifecycleOwner) { onOrOff ->
            addEditBtn.isEnabled = onOrOff
        }
        todoTitle.addTextChangedListener {
            viewmodel.title = it.toString()
            validateInputs()
        }
        todoDescription.addTextChangedListener {
            viewmodel.desc = it.toString()
            validateInputs()
        }

        addEditBtn.setOnClickListener {
            val clock: Clock = Clock.systemDefaultZone()
            val time = clock.instant().toString()
            val instantBefore = Instant.parse(time)
            lateinit var todo: Todo
            if (args.toDoItem != null) {
                 todo = Todo(
                    id = args.toDoItem!!.id,
                    title = viewmodel.title,
                    description = viewmodel.desc,
                    completed = false,
                    updated = instantBefore.toString(),
                    date = instantBefore.toString()
                )
                viewmodel.editTodoItem(todo)
                findNavController().navigateUp()
            }
            else {
                 todo = Todo(
                    title = viewmodel.title,
                    description = viewmodel.desc,
                    completed = false,
                    updated = instantBefore.toString(),
                    date = instantBefore.toString()
                )
                viewmodel.addTodoItem(todo)
                findNavController().navigateUp()
            }


        }
    }

    private fun validateInputs() {
        if (viewmodel.title.isNotEmpty() && viewmodel.desc.isNotEmpty()) {
            viewmodel.setContinueBtn(true)
        } else {
            viewmodel.setContinueBtn(false)

        }
    }
}
