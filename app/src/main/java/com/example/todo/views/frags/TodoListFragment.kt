package com.example.todo.views.frags

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todo.databinding.FragmentTodoListBinding
import com.example.todo.model.repo.RepoImplement
import com.example.todo.model.models.Todo
import com.example.todo.util.Resource
import com.example.todo.util.loggerToastFrag
import com.example.todo.viewmodels.TodoListViewModel
import com.example.todo.viewmodels.factories.ViewmodelFactoryTodoList
import com.example.todo.views.adapter.SwipeySwipeCallback
import com.example.todo.views.adapter.TodoAdapter


class TodoListFragment : Fragment() {

    private var _binding : FragmentTodoListBinding? = null
    private val binding: FragmentTodoListBinding get() = _binding!!

    private val repoImp by lazy { RepoImplement(requireContext()) }
    private val viewmodel by viewModels<TodoListViewModel>{ViewmodelFactoryTodoList(repoImp)}

    private val adapter = TodoAdapter(::itemListToDetailAdd, ::editTodoItem, ::deleteTodo)


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentTodoListBinding.inflate(inflater, container, false). also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onResume() = with(binding){
        deleteTodoBtn.setOnClickListener {
            todoRecycler.adapter = null
        }
        viewmodel.getAllTodos()
        super.onResume()
    }

    private fun initViews() = with(binding){
        todoRecycler.layoutManager = LinearLayoutManager(requireContext())
        todoRecycler.adapter = adapter
        val swipeCallback = SwipeySwipeCallback(adapter, viewmodel)
        val swipeController = ItemTouchHelper(swipeCallback)
        swipeController.attachToRecyclerView(todoRecycler)
        addTodoBtn.setOnClickListener{
        findNavController()
            .navigate(TodoListFragmentDirections.actionTodoListFragmentToTodoDetailFragment(toDoItem = null))
        }
        viewmodel.liveData.observe(viewLifecycleOwner) { viewState ->
            when(viewState) {
                is Resource.Idle -> {
                    loggerToastFrag("Idle State")
                }
                is Resource.Error -> {
                    loggerToastFrag(viewState.msg)
                }
                is Resource.Loading -> {
                    progCirc.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    progCirc.visibility = View.GONE
                    adapter.applyItemToList(viewState.data)

//                    val swipeCallback = AdapterItemTouchCallback(adapter)
//                    val swipeController = ItemTouchHelper(swipeCallback)
//                    swipeController.attachToRecyclerView(todoRecycler)
                }
            }
        }
    }

    private fun itemListToDetailAdd (todo : Todo) = with(binding){
        findNavController()
            .navigate(TodoListFragmentDirections.actionTodoListFragmentToTodoDetailFragment(todo))
        loggerToastFrag("Clicking through to Detail")
    }
    private fun editTodoItem(todo: Todo) {
        viewmodel.editTodoItem(todo)
    }
    private fun deleteTodo(todo: Todo) {
        viewmodel.deleteTodoItem(todo)
    }
}


