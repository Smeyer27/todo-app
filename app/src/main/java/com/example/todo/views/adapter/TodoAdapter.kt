package com.example.todo.views.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.databinding.TodoListItemBinding
import com.example.todo.model.models.Todo
import com.example.todo.util.listIDs

class TodoAdapter(
    private val itemListToDetailEdit: (todo : Todo) -> Unit,
    private val editTodoItem: (todo : Todo) -> Unit,
    private val deleteTodoItem: (todo : Todo) -> Unit
    ): RecyclerView.Adapter<TodoAdapter.TodoViewHolder>() {

    var todoList: List<Todo> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val binding =
            TodoListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TodoViewHolder(binding).apply {
            binding.root.setOnClickListener {
                val todo = todoList[adapterPosition]
                itemListToDetailEdit(todo)
            }
            binding.checkbox.setOnClickListener{
                val todo = todoList[adapterPosition]
                editTodoItem(todo)
            }
        }
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        holder.applyToCard(todoList[position])
    }

    override fun getItemCount(): Int {
        return todoList.size
    }

    class TodoViewHolder(private val binding: TodoListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun applyToCard(todo: Todo) = with(binding) {
            listIDs.add(todo.id.toString())
            todoNum.text = todo.id.toString()
            todoItem.text = todo.title
            todoItem.paint.isStrikeThruText = todo.completed
            checkbox.isChecked = todo.completed

        }
    }

    fun applyItemToList(items: List<Todo>) {
        todoList = items
        notifyDataSetChanged()
    }
    fun deleteTodo(todo : Todo) {
        deleteTodoItem(todo)
    }


}