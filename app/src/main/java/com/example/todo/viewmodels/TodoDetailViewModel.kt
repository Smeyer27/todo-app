package com.example.todo.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todo.model.repo.RepoImplement
import com.example.todo.model.models.Todo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TodoDetailViewModel(private val repo: RepoImplement): ViewModel() {

    var title : String = ""
    var desc : String = ""

    private val _continueLiveData = MutableLiveData<Boolean>(false)
    val continueLiveData : LiveData<Boolean> get() = _continueLiveData

    fun setContinueBtn(flag: Boolean){
        _continueLiveData.value = flag
    }

    fun addTodoItem (todo : Todo) = viewModelScope.launch(Dispatchers.Main) {
        repo.addTodo(todo)
    }
    fun editTodoItem (todo: Todo) = viewModelScope.launch(Dispatchers.Main) {
        repo.editTodo(todo)
    }
//    fun completeTodo (id : Int) = viewModelScope.launch(Dispatchers.Main) {
//        repo.completeTodo(id)
//    }
}