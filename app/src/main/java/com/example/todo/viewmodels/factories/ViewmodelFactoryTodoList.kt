package com.example.todo.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todo.model.repo.RepoImplement
import com.example.todo.viewmodels.TodoListViewModel

class ViewmodelFactoryTodoList(private val repo: RepoImplement) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TodoListViewModel(repo) as T
    }
}