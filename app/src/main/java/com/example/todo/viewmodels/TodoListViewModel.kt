package com.example.todo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todo.model.repo.RepoImplement
import com.example.todo.model.models.Todo
import com.example.todo.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TodoListViewModel(private val repo: RepoImplement): ViewModel() {

    private val _liveData = MutableLiveData<Resource<List<Todo>>>(Resource.Loading())
    val liveData : MutableLiveData<Resource<List<Todo>>> get() = _liveData

    init{
        getAllTodos()
    }

    fun getAllTodos() = viewModelScope.launch(Dispatchers.Main) {
        _liveData.value = repo.grabAllTodos()
    }

    fun editTodoItem(todo: Todo) = viewModelScope.launch(Dispatchers.Main) {
        getAllTodos()
        repo.editTodo(todo)
    }

    fun deleteTodoItem(todo : Todo) = viewModelScope.launch(Dispatchers.Main) {
        repo.deleteTodo(todo)
        getAllTodos()
    }

}