package com.example.todo.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todo.model.repo.RepoImplement
import com.example.todo.viewmodels.TodoDetailViewModel

class ViewmodelFactoryDetail(private val repo: RepoImplement) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TodoDetailViewModel(repo) as T
    }
}