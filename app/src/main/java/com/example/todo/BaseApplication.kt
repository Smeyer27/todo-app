package com.example.todo

import android.app.Application

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        FlipperProvider.init(this)
    }
}