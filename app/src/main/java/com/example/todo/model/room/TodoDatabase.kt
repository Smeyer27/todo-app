package com.example.todo.model.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.todo.model.models.Todo

@Database(entities = [Todo::class], exportSchema = false, version = 1)
abstract class TodoDatabase : RoomDatabase() {
    abstract fun todoDao() : ToDoDAO

    companion object {
        private const val DB_NAME = "TODO_DB"

        fun getDatabaseInstance(context: Context) : TodoDatabase {
            return Room.databaseBuilder(context, TodoDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration().build()
        }
    }
}