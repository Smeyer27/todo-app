package com.example.todo.model.room

import androidx.room.*
import com.example.todo.model.models.Todo

@Dao
interface ToDoDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTodoItem(todo: Todo)

    @Update
    suspend fun editTodoItem(todo: Todo)

    @Query("SELECT * FROM todo_item")
    suspend fun grabAllTodos(): List<Todo>

    @Delete
    suspend fun deleteTodoItem(todo :Todo)

}