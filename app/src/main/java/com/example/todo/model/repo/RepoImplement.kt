package com.example.todo.model.repo

import android.content.Context
import com.example.todo.model.models.Todo
import com.example.todo.model.room.TodoDatabase
import com.example.todo.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RepoImplement (context: Context) : TodoRepo {

    private val todoDao = TodoDatabase.getDatabaseInstance(context).todoDao()

    override suspend fun addTodo(todo: Todo) = withContext(Dispatchers.IO) {
        todoDao.addTodoItem(todo)
    }
    override suspend fun editTodo(todo: Todo): Unit = withContext(Dispatchers.IO) {
        val completed = todo.completed
        todoDao.editTodoItem(todo.copy(completed = !completed))
    }

    override suspend fun grabAllTodos(): Resource<List<Todo>> = withContext(Dispatchers.IO){
        return@withContext try{
            val response = todoDao.grabAllTodos()
            Resource.Success(response)
        }
        catch (e: Exception) {
            Resource.Error(e.localizedMessage ?: "Something is wrong")
        }
    }

    override suspend fun deleteTodo(todo: Todo) = withContext(Dispatchers.IO) {
        todoDao.deleteTodoItem(todo)
    }
//    override suspend fun completeTodo(id: Int) = withContext(Dispatchers.IO){
//        todoDao.completeTodo(id)
//    }
}