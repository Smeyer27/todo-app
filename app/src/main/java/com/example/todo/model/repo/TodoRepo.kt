package com.example.todo.model.repo

import com.example.todo.model.models.Todo
import com.example.todo.util.Resource

interface TodoRepo {

    suspend fun addTodo(todo: Todo)
    suspend fun editTodo(todo: Todo)
    suspend fun grabAllTodos() : Resource<List<Todo>>
//    suspend fun completeTodo(id: Int)
    suspend fun deleteTodo(todo :Todo)
}